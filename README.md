# LDAP-Sync-RocketChat

## Démmarage
Modifier le fichier ldap.php en fonction de votre identifiant et mot de passe LDAP & Rocket Chat puis uploadez le sur votre serveur WEB.
Il vous suffira de créer une tâche cron de type CURL pour faire appel au script et supprimer les comptes RocketChat, si les comptes AD n'existent plus. 

## Docker
Si vous ne disposez pas de serveur WEB, vous pouvez vous appuyer soit sur mon image Docker, soit sur ce Dockerfile pour générer votre propre image Docker embarquant Apache 2 - PHP 7.4 & LDAP.


### Depuis mon repository
```sh
docker run --name ldapphp -v /opt/docker/www/data:/var/www/html --restart unless-stopped -p 80:80 -d magness/phpldap:latest
```

### Via Dockerfile
```sh
docker build -t phpldap .
docker run --name ldapphp -v /opt/docker/www/data:/var/www/html --restart unless-stopped -p 80:80 -d phpldap:latest
```
