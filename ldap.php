<?php   

// récupération des Users LDAP 
$ldaprdn  = 'Administrator@local.tech';     // DN ou RDN LDAP
$ldappass = 'mypassword';  // Mot de passe associé

// Connexion au serveur LDAP
$ldapconn = ldap_connect("ldap://223.223.223.8")
    or die("Impossible de se connecter au serveur LDAP.");

if ($ldapconn) {
    // Connexion au serveur LDAP
    $ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);
    // Vérification de l'authentification
    if ($ldapbind) {
        $ldap_base_dn = 'OU=Rocketchat,DC=local,DC=tech'; // OU de recherche
        $search_filter = '(&(objectCategory=person)(samaccountname=*))'; // Filtre de recherche
        $attributes = array();
        $attributes[] = 'givenname';
        $attributes[] = 'mail';
        $attributes[] = 'samaccountname';
        $attributes[] = 'sn';
        $result = ldap_search($ldapconn, $ldap_base_dn, $search_filter, $attributes);
        if (FALSE !== $result){
            $entries = ldap_get_entries($ldapconn, $result);
            for ($x=0; $x<$entries['count']; $x++){
                $user[] = trim($entries[$x]['samaccountname'][0]);                
            }
        }
        ldap_unbind($ldapconn);
    } else {
        echo "Connexion LDAP échouée...";
    }
}

// Récupération des Users sur Rocket Chat
$headers = array();
$headers[] = "x-auth-token: YOUR TOKEN";
$headers[] = "X-User-Id: YOUR ID";
$headers[] = 'Content-Type: application/x-www-form-urlencoded; charset=utf-8';
$state_ch = curl_init();
curl_setopt($state_ch, CURLOPT_URL,"https://YOUR URL OR IP ADRESS/api/v1/users.list");
curl_setopt($state_ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($state_ch, CURLOPT_HTTPHEADER, $headers);
$data = curl_exec ($state_ch);
$b=json_decode($data);
$count = count($b->{'users'});
for ($i = 0; $i <= $count; $i++) {
    if(($b->{'users'}[$i]->{'roles'}[0]=='user') && ($b->{'users'}[$i]->{'roles'}[1]!='admin')){
        $key = array_search(($b->{'users'}[$i]->{'username'}), $user);
        if(is_bool($key) != true){
            
        }else{
            $post = ['username' => $b->{'users'}[$i]->{'username'}];
            curl_setopt($state_ch, CURLOPT_URL,"https://rocketchat.magness.fr/api/v1/users.delete");
            curl_setopt($state_ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($state_ch, CURLOPT_POSTFIELDS, http_build_query($post));
            curl_setopt($state_ch, CURLOPT_HTTPHEADER, $headers);
            $data = curl_exec ($state_ch);
            $d=json_decode($data);
            echo $b->{'users'}[$i]->{'username'} . ' a été supprimé de RocketChat<br>';
        }
    }
}

?>
